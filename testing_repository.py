import json
import sqlite3
from sqlite3 import Error

from logging.config import dictConfig


from singleton import Singleton
import logging
import yaml


#from utils import ApplicationPaths, LoggerFactory, Singleton, Utils

logging.config.dictConfig(yaml.load(open('logging.yaml', 'r'), Loader=yaml.SafeLoader))

logger = logging.getLogger("logger_application")

table_pbs_blocks = 'pbs_blocks'

sql_create_pbs_blocks_table = """ CREATE TABLE IF NOT EXISTS pbs_blocks (
                            block_number INTEGER NOT NULL,
                            block_hash TEXT NOT NULL,
                            slot INTEGER NOT NULL,
                            epoch INTEGER NOT NULL,
                            gas_limit INTEGER NOT NULL,
                            gas_used INTEGER NOT NULL,
                            parent_hash TEXT NOT NULL,
                            proposer_fee_recipient TEXT NOT NULL,
                            proposer_pubkey TEXT NOT NULL,
                            builder_pubkey TEXT NOT NULL,
                            builder_fee_recipient TEXT NOT NULL,
                            builder_public_name_tag TEXT NOT NULL,
                            builder_extra_data_decoded TEXT NOT NULL,
                            builder_extra_data_hex TEXT NOT NULL,
                            block_reward INTEGER NOT NULL,
                            static_block_reward INTEGER NOT NULL,
                            transaction_fees INTEGER NOT NULL,
                            burnt_fees INTEGER NOT NULL,
                            num_tx INTEGER NOT NULL,
                            value INTEGER NOT NULL
                            ); """


pbs_blocks_keys = ['block_number','block_hash', 'slot', 'epoch', 'gas_limit', 'gas_used', 'parent_hash', 'proposer_fee_recipient', 'proposer_pubkey', 'builder_pubkey' , \
    'builder_fee_recipient', 'builder_public_name_tag', 'builder_extra_data_decoded', 'builder_extra_data_hex', 'block_reward', 'static_block_reward', 'transaction_fees', \
    'burnt_fees', 'num_tx', 'value']

@Singleton
class RepositoryRDBMS:
    __conn = None

    def __init__(self):
        logger.info(">>> Connecting to the database")
        json_str = open("db_auth.json").read()
        db_auth = json.loads(json_str)
        # self.__conn = self.__create_connection(
        #    ApplicationPaths.database() + db_auth['sqlite']['db_file'])
        self.__conn = self.__create_connection(db_auth['sqlite']['db_file'])
        self.__create_table(sql_create_pbs_blocks_table)

    def __create_connection(self, db_file):
        # Create a SQLite database connection!
        conn = None
        try:
            conn = sqlite3.connect(db_file, check_same_thread=False)
            logger.info(f"Data base connected: SQLite {sqlite3.version}")
        except Error as e:
            logger.error(e)
        return conn

    def __create_table(self, table):
        try:
            c = self.__conn.cursor()
            c.execute(table)
            self.__conn.commit()
            logger.info(f"Table {table} created")
        except Error as e:
            logger.error(e)

    def insertData(self, block):
        data = [block[value] for value in pbs_blocks_keys]
        sql = f"INSERT INTO {table_pbs_blocks} ({','.join(pbs_blocks_keys)}) VALUES({','.join('?'*len(pbs_blocks_keys))});"
        self.__persist(sql=sql, data=data)

    def get_block_by_number(self, block_number):
        response = None
        sql = f"SELECT block_hash FROM {table_pbs_blocks} WHERE block_number = {block_number};"
        data = self.__select(sql=sql)
        if data:
            response = dict(zip(['block_hash'], data[0]))
        return response

    def __select(self, sql, fetchmany=False):
        response = {}
        try:
            # logger.info(sql)
            cursor = self.__conn.cursor()
            cursor.execute(sql)
            if fetchmany:
                response = cursor.fetchmany
            else:
                response = cursor.fetchall()
        except Error:
            logger.error('>>> __select', exc_info=True)
        return response

    def __persist(self, sql, data, is_batch=False, page_size=3000):
        try:
            cursor = self.__conn.cursor()
            if is_batch:
                cursor.executemany(sql, data)
            else:
                cursor.execute(sql, data)
            self.__conn.commit()
        except Error:
            logger.error('>>> __persist', exc_info=True)
    
    
#if __name__ == '__main__':
#    repository = RepositoryRDBMS()




