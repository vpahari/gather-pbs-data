#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sys
import time
import random
import logging
import decimal
#import pymongo
import requests
import multiprocessing
import undetected_chromedriver as uc

from web3 import Web3
from selenium.webdriver.chrome.options import Options

from testing_repository import RepositoryRDBMS 

class colors:
    INFO = "\033[94m"
    OK = "\033[92m"
    FAIL = "\033[91m"
    END = "\033[0m"

RELAY_ENDPOINTS = {
  "Flashbots"               : "https://boost-relay.flashbots.net",
  "bloXroute (MaxProfit)"   : "https://bloxroute.max-profit.blxrbdn.com",
  "bloXroute (Ethical)"     : "https://bloxroute.ethical.blxrbdn.com",
  "bloXroute (Regulated)"   : "https://bloxroute.regulated.blxrbdn.com",
  "Blocknative"             : "https://builder-relay-mainnet.blocknative.com",
  "Manifold"                : "https://mainnet-relay.securerpc.com",
  "Eden"                    : "https://relay.edennetwork.io",
  "UltraSound"              : "https://relay.ultrasound.money",
  "GnosisDAO"               : "https://agnostic-relay.net",
  "Aestus"                  : "https://aestus.live",
  "Relayooor"               : "https://relayooor.wtf"
}

MONGO_HOST = "sectrs-vascodagama"
MONGO_PORT = 27017

MAX_TIMEOUT = 20
MAX_RETRIES = 3

CPUs = 4

def get_validators(endpoint):
    res = None
    counter = 0
    while res == None:
        try:
            res = requests.get(endpoint+"/relay/v1/builder/validators", timeout=MAX_TIMEOUT)
            if res.status_code == 200:
                return res.json()
            else:
                logging.info(colors.FAIL+"Error: get_validators() "+endpoint+" "+str(block_number)+" "+str(res.status_code)+" "+str(res.content)+colors.END)
                raise
        except Exception as e:
            counter += 1
            if counter <= 3:
                logging.info(colors.FAIL+"Retrying get_validators() "+str(counter)+"/"+str(MAX_RETRIES)+" "+endpoint+" "+str(block_number)+" "+str(e)+colors.END)
                time.sleep(random.randint(1, MAX_RETRIES) * 60)
                res = None
    return None

def get_proposer_payloads_delivered(endpoint, block_number):
    res = None
    counter = 0
    while res == None:
        try:
            res = requests.get(endpoint+"/relay/v1/data/bidtraces/proposer_payload_delivered?block_number="+str(block_number), timeout=MAX_TIMEOUT)
            if res.status_code == 200:
                return res.json()
            elif endpoint == "https://relayooor.wtf" and res.status_code == 502:
                return list()
            else:
                logging.info(colors.FAIL+"Error: get_proposer_payloads_delivered() "+endpoint+" "+str(block_number)+" "+str(res.status_code)+" "+str(res.content)+colors.END)
                raise
        except Exception as e:
            counter += 1
            if counter <= 3:
                logging.info(colors.FAIL+"Retrying get_proposer_payloads_delivered() "+str(counter)+"/"+str(MAX_RETRIES)+" "+endpoint+" "+str(block_number)+" "+str(e)+colors.END)
                time.sleep(random.randint(1, MAX_RETRIES) * 60)
                res = None
    return None

def get_builder_blocks_received(endpoint, block_number):
    res = None
    counter = 0
    while res == None:
        try:
            res = requests.get(endpoint+"/relay/v1/data/bidtraces/builder_blocks_received?block_number="+str(block_number), timeout=MAX_TIMEOUT)
            if res.status_code == 200:
                return res.json()
            else:
                logging.info(colors.FAIL+"Error: get_builder_blocks_received() "+endpoint+" "+str(block_number)+" "+str(res.status_code)+" "+str(res.content)+colors.END)
                raise
        except Exception as e:
            counter += 1
            if counter <= 3:
                logging.info(colors.FAIL+"Retrying get_builder_blocks_received() "+str(counter)+"/"+str(MAX_RETRIES)+" "+endpoint+" "+str(block_number)+" "+str(e)+colors.END)
                time.sleep(random.randint(1, MAX_RETRIES) * 60)
                res = None
    return None

def get_validator_registration(endpoint, pubkey, etherscan_content, block_number):
    res = None
    counter = 0
    while res == None:
        try:
            res = requests.get(endpoint+"/relay/v1/data/validator_registration?pubkey="+pubkey, timeout=MAX_TIMEOUT)
            if res.status_code == 200:
                return res.json()
            elif res.status_code == 400 or res.status_code == 404:
                proposer_fee_recipient = re.compile("Proposer Fee Recipient:</div><div class=\"col-md-9\"><a href='/address/.+?'>(.+?)</a>").findall(etherscan_content.replace("\n", ""))
                proposer_pubkey = get_proposer_public_key(etherscan_content, block_number)
                if len(proposer_fee_recipient) > 0:
                    if proposer_pubkey != None:
                        return {"fee_recipient": proposer_fee_recipient[0], "pubkey": proposer_pubkey}
                    else:
                        return None
                else:
                    return {"fee_recipient": "", "pubkey": proposer_pubkey}
            elif res.status_code == 500:
                proposer_pubkey = get_proposer_public_key(etherscan_content, block_number)
                if proposer_pubkey != None:
                    return {"fee_recipient": "", "pubkey": proposer_pubkey}
                else:
                    return None
            else:
                logging.info(colors.FAIL+"Error: get_validator_registration() "+endpoint+" "+pubkey+" "+str(block_number)+" "+str(res.status_code)+" "+str(res.content)+colors.END)
                raise
        except Exception as e:
            counter += 1
            if counter <= 3:
                logging.info(colors.FAIL+"Retrying get_validator_registration() "+str(counter)+"/"+str(MAX_RETRIES)+" "+endpoint+" "+pubkey+" "+str(block_number)+" "+str(e)+colors.END)
                time.sleep(random.randint(1, MAX_RETRIES) * 60)
                res = None
    return None

def get_proposer_public_key(etherscan_content, block_number):
    proposer_index = re.compile("Proposer Index:</div><div class=\"col-md-9\"><a href=\"https://beaconscan.com/validator/(.+?)\"").findall(etherscan_content.replace("\n", ""))
    if len(proposer_index) > 0:
        beaconscan_content = get_cloudflare_content("https://beaconscan.com/validator/"+proposer_index[0])
        proposer_pubkey = re.compile("<span class=\"mr-1\">0x(.+?)</span>").findall(beaconscan_content.replace("\n", ""))
        if len(proposer_pubkey) > 0:
            return "0x"+proposer_pubkey[0]
        else:
            logging.info(colors.FAIL+"Error: Could not extract proposer public key from Beaconscan ("+str(block_number)+")."+colors.END)
            return None
    else:
        logging.info(colors.FAIL+"Error: Could not extract proposer index from Etherscan ("+str(block_number)+")."+colors.END)
        return None

def get_cloudflare_content(url):
    driver.get(url)
    content = driver.page_source
    while not content or "Enable JavaScript and cookies to continue" in content or "Maintenance Mode" in content:
        content = driver.page_source
        if not content:
            driver.get(url)
        elif "Maintenance Mode" in content:
            logging.info(colors.FAIL+"Etherscan is rate limiting us..."+colors.END)
            time.sleep(5)
            driver.get(url)
    return content

def analyze_block(block_number):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    logging.basicConfig(
        format='%(asctime)s %(process)d %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S'
    )

    logging.info("Analyzing block: "+colors.INFO+str(block_number)+colors.END)

    # Check if block has already been analyzed
    #block_analyzed = mongo_connection["pbs_analysis"]["relay_blocks"].find_one({"block_number": block_number})
    block_analyzed = pbs_repo.get_block_by_number(block_number)
    if block_analyzed is None:
        return

    block = dict()
    content = get_cloudflare_content("https://etherscan.io/block/"+str(block_number))

    builder_fee_recipient = re.compile('<a href="/address/(.+?)" data-bs-toggle="tooltip" data-bs-trigger="hover">(.+?)</a>').findall(content)
    if len(builder_fee_recipient) == 0:
        builder_fee_recipient = re.compile('<a href="/address/(.+?)" class="hash-tag text-truncate d-inline me-1" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-html="true" data-bs-title=".+?">(.+?)</a>').findall(content)
        if len(builder_fee_recipient) == 0:
            builder_fee_recipient = re.compile('<a href="/address/(.+?)">.+?</a>').findall(content)
            if len(builder_fee_recipient) > 0:
                block["builder_fee_recipient"] = builder_fee_recipient[0]
                block["builder_public_name_tag"] = ""
            else:
                logging.info(colors.FAIL+"Error: Could not extract builder fee recipient from Etherscan ("+str(block_number)+")."+colors.END)
                return
        else:
            block["builder_fee_recipient"] = builder_fee_recipient[0][0]
            block["builder_public_name_tag"] = builder_fee_recipient[0][1]
    else:
        block["builder_fee_recipient"] = builder_fee_recipient[0][0]
        block["builder_public_name_tag"] = builder_fee_recipient[0][1]

    block["payloads_delivered_to_proposers"] = list()
    block["blocks_proposed_by_builders"] = dict()

    # Scan each relay for blocks
    relays = list()
    found_payload = False
    for relay in RELAY_ENDPOINTS:
        proposer_payloads_delivered = get_proposer_payloads_delivered(RELAY_ENDPOINTS[relay], block_number)
        if proposer_payloads_delivered == None:
            return
        if len(proposer_payloads_delivered) > 0:
            relays.append(relay)
            if found_payload == True:
                logging.info(colors.INFO+"Info: block "+str(block_number)+" has multiple relays! ("+str(relays)+")"+colors.END)
            found_payload = True
            logging.info("Block "+colors.INFO+str(block_number)+colors.END+" has "+colors.INFO+str(len(proposer_payloads_delivered))+colors.END+" payload(s) from relay: "+colors.INFO+relay+colors.END)
            if len(proposer_payloads_delivered) > 1:
                logging.info(colors.FAIL+"Error: block "+str(block_number)+" has more than one payload!"+colors.END)
            for payload in proposer_payloads_delivered:
                payload["relay"] = relay
                block["payloads_delivered_to_proposers"].append(payload)
                proposer_pubkey = payload["proposer_pubkey"]
                builder_pubkey = payload["builder_pubkey"]
                proposer = get_validator_registration(RELAY_ENDPOINTS[relay], proposer_pubkey, content, block_number)
                if proposer == None:
                    return
                if proposer:
                    if "message" in proposer:
                        proposer = proposer["message"]
                    logging.info(" -> Payload was proposed by "+colors.INFO+proposer["fee_recipient"]+colors.END+" (pubkey: "+colors.INFO+proposer["pubkey"]+colors.END+")")
                    block["proposer_fee_recipient"] = proposer["fee_recipient"]
                    block["proposer_pubkey"] = proposer["pubkey"]
                if builder_pubkey:
                    logging.info(" -> Payload was built by "+colors.INFO+block["builder_fee_recipient"]+colors.END+" (pubkey: "+colors.INFO+builder_pubkey+colors.END+")")
                    builder_blocks_received = get_builder_blocks_received(RELAY_ENDPOINTS[relay], block_number)
                    if builder_blocks_received == None:
                        return
                    if not relay in block["blocks_proposed_by_builders"]:
                        block["blocks_proposed_by_builders"][relay] = list()
                    logging.info(" -> Parsing "+colors.INFO+str(len(builder_blocks_received))+colors.END+" blocks proposed by builders for block "+colors.INFO+str(block_number)+colors.END)
                    for received_builder_block in builder_blocks_received:
                        del received_builder_block["proposer_fee_recipient"]
                        del received_builder_block["proposer_pubkey"]
                        del received_builder_block["parent_hash"]
                        del received_builder_block["block_number"]
                        del received_builder_block["gas_limit"]
                        del received_builder_block["slot"]
                        if "timestamp_ms" in received_builder_block:
                            received_builder_block["timestamp"] = received_builder_block["timestamp_ms"]
                            del received_builder_block["timestamp_ms"]
                        received_builder_block["gas_used"] = int(received_builder_block["gas_used"])
                        received_builder_block["num_tx"] = int(received_builder_block["num_tx"])
                        received_builder_block["timestamp"] = int(received_builder_block["timestamp"])
                        block["blocks_proposed_by_builders"][relay].append(received_builder_block)

    block["block_number"] = block_number
    block["relays"] = relays

    beacon_chain_info = re.compile("Block proposed on slot <a href=\"https://beaconscan.com/slot/.+?\" target=\"_blank\">(.+?)</a>, epoch <a href=\"https://beaconscan.com/epoch/.+?\" target=\"_blank\">(.+?)</a>").findall(content)
    if len(beacon_chain_info) > 0:
        block["slot"] = int(beacon_chain_info[0][0])
        block["epoch"] = int(beacon_chain_info[0][1])
    else:
        logging.info(colors.FAIL+"Error: Could not extract slot and epoch from Etherscan ("+str(block_number)+")."+colors.END)
        return

    if not "block_hash" in block:
        block_hash = re.compile("Hash:</div><div class=\".+?\">(.+?)</div>").findall(content.replace("\n", ""))
        if len(block_hash) > 0:
            block["block_hash"] = block_hash[0]
        else:
            logging.info(colors.FAIL+"Error: Could not extract block hash from Etherscan ("+str(block_number)+")."+colors.END)
            return

    if not "parent_hash" in block:
        parent_hash = re.compile("Parent Hash:</div><div class=\".+?\"><a href=\"/block/.+?\">(.+?)</a></div>").findall(content.replace("\n", ""))
        if len(parent_hash) > 0:
            block["parent_hash"] = parent_hash[0]
        else:
            logging.info(colors.FAIL+"Error: Could not extract parent hash from Etherscan ("+str(block_number)+")."+colors.END)
            return

    if not "gas_limit" in block:
        gas_limit = re.compile("Gas Limit:</div><div class=\".+?\">(.+?)</div>").findall(content.replace("\n", "").replace(",", ""))
        if len(gas_limit) > 0:
            block["gas_limit"] = int(gas_limit[0])
        else:
            logging.info(colors.FAIL+"Error: Could not extract gas limit from Etherscan ("+str(block_number)+")."+colors.END)
            return

    if not "gas_used" in block:
        gas_used = re.compile("Gas Used:</div><div class=\".+?\">(.+?) .+? \(.+?%\)").findall(content.replace("\n", "").replace(",", ""))
        if len(gas_used) > 0:
            block["gas_used"] = int(gas_used[0])
        else:
            logging.info(colors.FAIL+"Error: Could not extract gas used from Etherscan ("+str(block_number)+")."+colors.END)
            return

    builder_extra_data = re.compile("Extra Data:</div><div class=\".+?\">(.+?) \(Hex:(.+?)\)</div>").findall(content.replace("\n", ""))
    if len(builder_extra_data) > 0:
        if builder_extra_data[0][0] == "0x":
            block["builder_extra_data_decoded"] = ""
        else:
            block["builder_extra_data_decoded"] = builder_extra_data[0][0]
        if builder_extra_data[0][1] == "Null":
            block["builder_extra_data_hex"] = ""
        else:
            block["builder_extra_data_hex"] = builder_extra_data[0][1]
    else:
        logging.info(colors.FAIL+"Error: Could not extract builder extra data from Etherscan ("+str(block_number)+")."+colors.END)
        return

    block_reward = re.compile("Block Reward:</div><div class=\".+?\">(.+?) ETH").findall(content.replace("\n", "").replace("<b>", "").replace("</b>", ""))
    if len(block_reward) > 0:
        block_reward = decimal.Decimal(block_reward[0])
    else:
        logging.info(colors.FAIL+"Error: Could not extract block reward from Etherscan ("+str(block_number)+")."+colors.END)
        return
    block["block_reward"] = str(Web3.toWei(block_reward, 'ether'))

    static_block_reward = re.compile('\(<span rel="tooltip" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom">(.+?)</span> \+').findall(content.replace("\n", ""))
    if len(static_block_reward) > 0:
        static_block_reward = decimal.Decimal(static_block_reward[0])
    else:
        logging.info(colors.FAIL+"Error: Could not extract static block reward from Etherscan ("+str(block_number)+")."+colors.END)
        return
    block["static_block_reward"] = str(Web3.toWei(static_block_reward, 'ether'))

    transaction_fees = re.compile('\+ <span rel="tooltip" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom">(.+?)</span>').findall(content.replace("\n", ""))
    if len(transaction_fees) > 0:
        transaction_fees = decimal.Decimal(transaction_fees[0])
    else:
        logging.info(colors.FAIL+"Error: Could not extract transaction fees from Etherscan ("+str(block_number)+")."+colors.END)
        return
    block["transaction_fees"] = str(Web3.toWei(transaction_fees, 'ether'))

    burnt_fees = re.compile('<span rel="tooltip" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom"> - (.+?)</span>\)').findall(content.replace("\n", ""))
    if len(burnt_fees) > 0:
        burnt_fees = decimal.Decimal(burnt_fees[0])
    else:
        logging.info(colors.FAIL+"Error: Could not extract burnt fees from Etherscan ("+str(block_number)+")."+colors.END)
        return
    block["burnt_fees"] = str(Web3.toWei(burnt_fees, 'ether'))

    if block_reward != static_block_reward + transaction_fees - burnt_fees:
        logging.info(colors.FAIL+"Error: block reward does not match static block reward, transaction fees, and burnt fees ("+str(block_number)+")."+colors.END)
        return

    if not "num_tx" in block:
        num_tx = re.compile('data-bs-toggle="tooltip" data-bs-trigger="hover">([0-9]+) transaction.+?</a>').findall(content.replace("\n", ""))
        if len(num_tx) > 0:
            block["num_tx"] = int(num_tx[0])
        elif "0 transaction and 0 contract internal transaction in this block" in content.replace("\n", ""):
            block["num_tx"] = 0
        else:
            logging.info(colors.FAIL+"Error: Could not extract number of transactions from Etherscan ("+str(block_number)+")."+colors.END)
            return

    if not "builder_pubkey" in block:
        block["builder_pubkey"] = ""

    if not "proposer_pubkey" in block:
        proposer_public_key = get_proposer_public_key(content, block_number)
        if proposer_public_key != None:
            block["proposer_pubkey"] = proposer_public_key
        else:
            logging.info(colors.FAIL+"Error: Could not extract proposer public key Etherscan ("+str(block_number)+")."+colors.END)
            return

    if not "proposer_fee_recipient" in block:
        block["proposer_fee_recipient"] = ""

    if not "relays" in block:
        block["relays"] = list()

    if not "value" in block:
        block["value"] = ""

    # Search within payloads for remaining info
    for payload in block["payloads_delivered_to_proposers"]:
        if payload["proposer_pubkey"] == block["proposer_pubkey"]:
            block["builder_pubkey"] = payload["builder_pubkey"]
            block["proposer_fee_recipient"] = payload["proposer_fee_recipient"]
            block["value"] = payload["value"]

    logging.info(colors.OK+str(block["block_number"])+colors.END)

    #collection = mongo_connection["pbs_analysis"]["relay_blocks"]
    pbs_repo.insertData(block)
    
    '''
    if "block" not in collection.index_information():
        collection.create_index("block_number", unique=True)
        collection.create_index("block_hash", unique=True)
        collection.create_index("slot", unique=True)
        collection.create_index("epoch")
        collection.create_index("gas_limit")
        collection.create_index("gas_used")
        collection.create_index("parent_hash")
        collection.create_index("proposer_fee_recipient")
        collection.create_index("proposer_pubkey")
        collection.create_index("builder_pubkey")
        collection.create_index("builder_fee_recipient")
        collection.create_index("builder_public_name_tag")
        collection.create_index("builder_extra_data_decoded")
        collection.create_index("builder_extra_data_hex")
        collection.create_index("block_reward")
        collection.create_index("static_block_reward")
        collection.create_index("transaction_fees")
        collection.create_index("burnt_fees")
        collection.create_index("num_tx")
        collection.create_index("value")
    '''

def init_process():
    global driver
    #global mongo_connection
    global pbs_repo

    options = Options()
    options.add_argument("--headless=new")
    driver = uc.Chrome(options=options)

    #mongo_connection = pymongo.MongoClient("mongodb://"+MONGO_HOST+":"+str(MONGO_PORT), maxPoolSize=None)
    pbs_repo = RepositoryRDBMS.Instance()

def main():
    # Fist block after the merge:
    # https://etherscan.io/block/15537394
    # 3 months later:
    # https://etherscan.io/block/16187394

    logging.basicConfig(
        format='%(asctime)s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S'
    )

    if len(sys.argv) != 2:
        logging.info(colors.FAIL+"Error: Please provide a block range to be analyzed: 'python3 "+sys.argv[0]+" <BLOCK_RANGE_START>:<BLOCK_RANGE_END>'"+colors.END)
        sys.exit(-1)
    if not ":" in sys.argv[1]:
        logging.info(colors.FAIL+"Error: Please provide a valid block range: 'python3 "+sys.argv[0]+" <BLOCK_RANGE_START>:<BLOCK_RANGE_END>'"+colors.END)
        sys.exit(-2)
    block_range_start, block_range_end = sys.argv[1].split(":")[0], sys.argv[1].split(":")[1]
    if not block_range_start.isnumeric() or not block_range_end.isnumeric():
        logging.info(colors.FAIL+"Error: Please provide integers as block range: 'python3 "+sys.argv[0]+" <BLOCK_RANGE_START>:<BLOCK_RANGE_END>'"+colors.END)
        sys.exit(-3)
    block_range_start, block_range_end = int(block_range_start), int(block_range_end)

    if sys.platform.startswith("linux"):
        #multiprocessing.set_start_method("fork")
        ctx = multiprocessing.get_context("fork")
        #ctx.set_start_method("fork")
    logging.info("Analyzing blocks using "+colors.INFO+str(CPUs)+colors.END+" CPUs and "+colors.INFO+str(len(RELAY_ENDPOINTS))+colors.END+" relay endpoints")
    #with multiprocessing.Pool(processes=CPUs, initializer=init_process, initargs=()) as pool:
    with ctx.Pool(processes=CPUs, initializer=init_process, initargs=()) as pool:
        pool.map(analyze_block, range(block_range_start, block_range_end+1))

    if sys.platform.startswith("linux"):
        os.system("pkill chrome")
    if sys.platform.startswith("darwin"):
        os.system("killall 'Google Chrome'")

    logging.info("Done.")

if __name__ == "__main__":
    main()
